# django-wellknown-security

- Build egg
```
(py38_venv) peter@peter-desktop:~/github/django-wellknown-security$ git pull origin master --allow-unrelated-histories
From gitlab.com:peter.gastinger/django-wellknown-security
 * branch            master     -> FETCH_HEAD
^C
(py38_venv) peter@peter-desktop:~/github/django-wellknown-security$ ^C
(py38_venv) peter@peter-desktop:~/github/django-wellknown-security$ ^C
(py38_venv) peter@peter-desktop:~/github/django-wellknown-security$ python setup.py sdist
running sdist
running egg_info
writing django_wellknown_security.egg-info/PKG-INFO
writing dependency_links to django_wellknown_security.egg-info/dependency_links.txt
writing top-level names to django_wellknown_security.egg-info/top_level.txt
reading manifest file 'django_wellknown_security.egg-info/SOURCES.txt'
reading manifest template 'MANIFEST.in'
writing manifest file 'django_wellknown_security.egg-info/SOURCES.txt'
running check
creating django-wellknown-security-0.1
creating django-wellknown-security-0.1/django_wellknown_security.egg-info
creating django-wellknown-security-0.1/wellknown_security
creating django-wellknown-security-0.1/wellknown_security/migrations
copying files to django-wellknown-security-0.1...
copying LICENSE -> django-wellknown-security-0.1
copying MANIFEST.in -> django-wellknown-security-0.1
copying README.rst -> django-wellknown-security-0.1
copying setup.cfg -> django-wellknown-security-0.1
copying setup.py -> django-wellknown-security-0.1
copying django_wellknown_security.egg-info/PKG-INFO -> django-wellknown-security-0.1/django_wellknown_security.egg-info
copying django_wellknown_security.egg-info/SOURCES.txt -> django-wellknown-security-0.1/django_wellknown_security.egg-info
copying django_wellknown_security.egg-info/dependency_links.txt -> django-wellknown-security-0.1/django_wellknown_security.egg-info
copying django_wellknown_security.egg-info/top_level.txt -> django-wellknown-security-0.1/django_wellknown_security.egg-info
copying wellknown_security/__init__.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/admin.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/apps.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/models.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/tests.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/urls.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/views.py -> django-wellknown-security-0.1/wellknown_security
copying wellknown_security/migrations/__init__.py -> django-wellknown-security-0.1/wellknown_security/migrations
Writing django-wellknown-security-0.1/setup.cfg
Creating tar archive
removing 'django-wellknown-security-0.1' (and everything under it)
```
- Push to pypi

```
(py38_venv) peter@peter-desktop:~/github/django-wellknown-security$ twine upload dist/*
Uploading distributions to https://upload.pypi.org/legacy/
Uploading django-wellknown-security-0.13.tar.gz
100%|███████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 10.0k/10.0k [00:01<00:00, 6.02kB/s]

View at:
https://pypi.org/project/django-wellknown-security/0.13/
```
