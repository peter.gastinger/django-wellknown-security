.. wellknown security.txt documentation master file, created by
   sphinx-quickstart on Mon Sep 21 08:25:12 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to wellknown security.txt's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Django wellknown security - work in progress ...

https://gitlab.com/peter.gastinger/django-wellknown-security
